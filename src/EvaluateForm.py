from tkinter import *
from tkinter import messagebox
from RulesEngine import RulesEngine
import MySQLdb


class EvaluateForm(object):
    value = "empty"

    def __init__(self):
        print("Evaluation form init")

    def buttonpress(self, function, *args):
        self.value = function(*args)
        messagebox.showinfo(title="Rule Output", message=self.value)
        db = MySQLdb.connect("sql9.freemysqlhosting.net", "sql9239369", "IWquP2KRf6", "sql9239369")

        cursor = db.cursor()
        print("update PhysicianRecord set `Analysis` = '%s" % self.value + "' where NPI_Number = %s" % args[0])
        cursor.execute("update PhysicianRecord set `Analysis` = '%s" % self.value + "' where NPI_Number = %s" % args[0])
        cursor.close()
        db.commit()
        db.close()

    def showForm(self):
        root = Tk()
        root.geometry("600x600")
        root.title("FInd your Physician!")

        Label_0 = Label(root, text="Evaluate", width=20, font=("bold", 20))
        Label_0.place(x=90, y=53)

        Label_1 = Label(root, text="NPI Number", width=20, font=("bold", 10))
        Label_1.place(x=80, y=130)

        entry_1 = Entry(root)
        entry_1.place(x=240, y=130)

        ruleEngine = RulesEngine()
        Button(root, text='Evaluate', width=20, bg='brown', fg='white',
               command= lambda: self.buttonpress(ruleEngine.rules_apply, entry_1.get())).place(
            x=180, y=530)

        root.mainloop()
