from tkinter import *

from AddRecordForm import AddRecordForm
from EvaluateForm import EvaluateForm

gui_instance = AddRecordForm()
evaluate_form = EvaluateForm()
root= Tk()
root.geometry("350x300")
root.title("FInd your Physician!")

Label_0= Label(root, text="Find your Physician!", width=20,font=("bold",20))
Label_0.place(x=40,y=20)

Button(root, text='Add Record', width=20, bg='brown', fg='white',command=gui_instance.showForm).place(x=100, y=100)
Button(root, text='Begin Evaluation', width=20, bg='brown', fg='white', command = evaluate_form.showForm).place(x=100, y=150)

root.mainloop()