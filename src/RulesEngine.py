import MySQLdb
from rulengine.core import DataType, RuleOperator, ConditionOperator, Rule, Condition
from rulengine import execute

class RulesEngine(object):
    def __info__(self):
        print("initializing rules engine ......")

    def rules_apply(self, npi_number):
        db = MySQLdb.connect("sql9.freemysqlhosting.net", "sql9239369", "IWquP2KRf6", "sql9239369")

        cursor = db.cursor()
        print("select * from PhysicianRecord where NPI_Number = %s" % npi_number)
        cursor.execute("select * from PhysicianRecord where NPI_Number = %s" % str(npi_number))
        rs = cursor.fetchall()
        print(rs[0])

        condition = Condition(value=int(rs[0][3]), operator=ConditionOperator.GREATER, comparison_value=35, data_type=DataType.INTEGER)
        condition1 = Condition(value=int(rs[0][3]), operator=ConditionOperator.LESS, comparison_value=50, data_type=DataType.INTEGER)
        rule = Rule(operator=RuleOperator.AND, conditions=[condition,condition1])

        condition2 = Condition(value=int(rs[0][3]), operator=ConditionOperator.GREATER, comparison_value=51, data_type=DataType.INTEGER)
        condition3 = Condition(value=int(rs[0][3]), operator=ConditionOperator.LESS, comparison_value=60, data_type=DataType.INTEGER)
        rule1 = Rule(operator=RuleOperator.AND, conditions=[condition2,condition3])

        condition4 = Condition(value=int(rs[0][3]), operator=ConditionOperator.GREATER, comparison_value=61,data_type=DataType.INTEGER)
        rule2 = Rule(operator=RuleOperator.AND, conditions=[condition4])

        db.commit()
        cursor.close()
        db.close()
        if execute([rule]):
            return "Young"
        elif execute([rule1]):
            return "Older"
        elif execute([rule2]):
            return "Old!"