from tkinter import *
import MySQLdb


class AddRecordForm(object):
    value = "empty now"
    def __init__(self):
        print("Record form init")

    def show_data(self, entry_1, entry_2, entry_3, entry_4, entry_5, entry_6, entry_7, entry_8):
        txtNPI = entry_1.get()
        txtFirstName = entry_2.get()
        txtLastName = entry_3.get()
        txtAge = entry_4.get()
        txtboard = entry_5.get()
        txtTelephone = entry_6.get()
        txtEmail = entry_7.get()
        txtProficiency = entry_8.get()

        db = MySQLdb.connect("sql9.freemysqlhosting.net", "sql9239369", "IWquP2KRf6", "sql9239369")

        cursor = db.cursor()
        print(
            "insert into PhysicianRecord (NPI_Number, FirstName,LastName ,Age,  BoardCertified, telephone , Email ,Proficiency) VALUES ",
            (txtNPI, txtFirstName, txtLastName, txtAge, txtboard, txtTelephone, txtEmail, txtProficiency))
        cursor.execute(
            "insert into PhysicianRecord (NPI_Number, FirstName,LastName ,Age,  BoardCertified, telephone , Email ,Proficiency) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            (str(txtNPI), txtFirstName, txtLastName, str(txtAge), txtboard, txtTelephone, txtEmail, txtProficiency))

        db.commit()
        cursor.close()
        db.close()

    def buttonpress(self, function, *args):
        self.value = function(*args)

    def showForm(self):
        root = Tk()
        root.geometry("600x600")
        root.title("FInd your Physician!")

        Label_0 = Label(root, text="Registration Form", width=20, font=("bold", 20))
        Label_0.place(x=90, y=53)

        Label_1 = Label(root, text="NPI Number", width=20, font=("bold", 10))
        Label_1.place(x=80, y=130)

        entry_1 = Entry(root)
        entry_1.place(x=240, y=130)

        Label_2 = Label(root, text="First Name", width=20, font=("bold", 10))
        Label_2.place(x=68, y=180)

        entry_2 = Entry(root)
        entry_2.place(x=240, y=180)

        Label_3 = Label(root, text="Last Name", width=20, font=("bold", 10))
        Label_3.place(x=68, y=230)

        entry_3 = Entry(root)
        entry_3.place(x=240, y=230)

        Label_4 = Label(root, text="Age", width=20, font=("bold", 10))
        Label_4.place(x=68, y=280)

        entry_4 = Entry(root)
        entry_4.place(x=240, y=280)

        Label_5 = Label(root, text="Board Certified", width=20, font=("bold", 10))
        Label_5.place(x=68, y=330)

        entry_5 = Entry(root)
        entry_5.place(x=240, y=330)

        Label_6 = Label(root, text="Telephone", width=20, font=("bold", 10))
        Label_6.place(x=68, y=380)

        entry_6 = Entry(root)
        entry_6.place(x=240, y=380)

        Label_7 = Label(root, text="Email ID", width=20, font=("bold", 10))
        Label_7.place(x=68, y=430)

        entry_7 = Entry(root)
        entry_7.place(x=240, y=430)

        Label_8 = Label(root, text="Proficiency", width=20, font=("bold", 10))
        Label_8.place(x=68, y=480)

        entry_8 = Entry(root)
        entry_8.place(x=240, y=480)

        Button(root, text='Add Record', width=20, bg='brown', fg='white',
               command= lambda: self.buttonpress(self.show_data,entry_1,entry_2,entry_3,entry_4,entry_5,entry_6,entry_7,entry_8)).place(
            x=180, y=530)
        '''print (self.value)
        Button(root, text='Close', width=20, bg='brown', fg='white',
               command=root.destroy).place(
            x=370, y=530)'''

        root.mainloop()
